package utils;

import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

/**
 *  Class with methods for data cleaning;
 *
 */
public final class ClearData {

    /**
     * Method cleans data in ui components;
     * @param textFields - components which will be cleaned;
     */
    public static void clearData(TextField... textFields){
        for (TextField field : textFields) {
            field.clear();
        }
    }

    /**
     * Method cleans data in ui components;
     * @param comboBoxes - components which will be cleaned;
     */
    public static void clearData(ComboBox... comboBoxes){
        for (ComboBox comboBox : comboBoxes) {
            comboBox.getSelectionModel().clearSelection();
        }
    }

    /**
     * Method cleans data in ui components;
     * @param checkBoxes - components which will be cleaned;
     */
    public static void clearData(CheckBox... checkBoxes){
        for (CheckBox checkBox : checkBoxes) {
            checkBox.setSelected(false);
        }
    }
}
