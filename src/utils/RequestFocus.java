package utils;

import javafx.scene.Node;
import javafx.scene.input.KeyCode;

/**
 * The class has method to working with focus component;
 */
public final class RequestFocus {

    /**
     * if the key ENTER on component node1 is pressed, the focus installs on node2;
     * @param node1
     * @param node2
     */
    public static void setRequestFocusOnKeyPressed(Node node1, Node node2){
        node1.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER)){
                node2.requestFocus();
            }
        });
    }
}
