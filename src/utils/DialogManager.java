package utils;

import javafx.scene.control.Alert;

/**
 * the class has methods for displaying dialog boxes(e.g. Error dialogs);
 *
 */
public final class DialogManager {

    /**
     * the method displays error dialog
     * @param title which will be displayed
     * @param text which will be displayed
     */
    public static  void showErrorDialog(String title, String text){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setContentText(text);
        alert.setHeaderText(title);
        alert.showAndWait();
    }

    /**
     * the method displays info dialog
     * @param title which will be displayed
     * @param text which will be displayed
     */
    public static void showInfoDialog(String title, String text){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setContentText(text);
        alert.setHeaderText(title);
        alert.showAndWait();
    }
}
