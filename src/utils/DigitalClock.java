package utils;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.Duration;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Creates a digital clock display as a simple label.
 * Format of the clock display is hh:mm:ss aa, where:
 * hh Hour in am/pm (1-12)
 * mm Minute in hour
 * ss Second in minute
 * aa Am/pm marker
 * Time is the system time for the local timezone.
 */
public final class DigitalClock extends Label{

    /**
     * In constructor runs method bindToTime();
     */
    public DigitalClock() {
        bindToTime();
    }

    /**
     * method sets properties;
     * @param layoutX - x;
     * @param layoutY - y;
     * @param prefWidth - width;
     * @param prefHeight - height;
     * @return digital clock with those properties;
     */
    public DigitalClock setProperties(double layoutX, double layoutY,
                               double prefWidth, double prefHeight) {
        this.setAlignment(Pos.CENTER);
        this.setLayoutX(layoutX);
        this.setLayoutY(layoutY);
        this.setPrefSize(prefWidth, prefHeight);
        this.setTextFill(Color.RED);
        this.setFont(new Font("Verdana", 18));
        this.setStyle("-fx-background-color: black");

        return this;
    }

    /**
     * The digital clock updates once a second.
     */
    private void bindToTime() {
        Timeline timeline = new Timeline(
                new KeyFrame(Duration.seconds(0),
                        event -> {
                            Calendar time = Calendar.getInstance();
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
                            setText(simpleDateFormat.format(time.getTime()));
                        }
                ),
                new KeyFrame(Duration.seconds(1))
        );
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }
}
