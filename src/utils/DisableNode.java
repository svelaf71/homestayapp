package utils;

import javafx.scene.Node;

/**
 * the class has methods for disabling or switching on nodes
 */
public final class DisableNode {

    /**
     * the methods disables nodes if @param disableNode is true, else method switches on nodes
     * @param disableNode
     * @param nodes
     */
    public static void setDisableNode(boolean disableNode, Node... nodes){
        for (Node node : nodes) {
            node.setDisable(disableNode);
        }
    }
}
