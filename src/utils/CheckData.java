package utils;

import javafx.scene.Node;
import javafx.scene.control.*;

/**
 * Class with methods for data checking and style setting;
 */
public final class CheckData {

    /**
     * Those constants are used for setting style
     */
    private static final String BORDER_RED = "-fx-border-color: red";
    private static final String BORDER_GREEN = "-fx-border-color: green";

    /**
     * Method checks data in ui components and sets style;
     * @param textField - components which will be checked;
     * @return - if data is correct then return true, else return false;
     */
    public static boolean checkData(TextField... textField){

        int result = 0;

        for (TextField field : textField) {
            if (field.getText().trim().length() <= 0){
                setStyle(field, BORDER_RED);
                field.requestFocus();
            }else {
                setStyle(field, BORDER_GREEN);
                result++;
            }
        }

        return result == textField.length;

    }

    /**
     * Method checks data in ui components and sets style;
     * @param comboBox - components which will be checked;
     * @return - if data is correct then return true, else return false;
     */
    public static boolean checkData(ComboBox... comboBox){

        int result = 0;

        for (ComboBox box : comboBox) {
            if (box.getSelectionModel().getSelectedItem() == null){
                setStyle(box, BORDER_RED);
                box.requestFocus();
            }else {
                setStyle(box, BORDER_GREEN);
                result++;
            }
        }

        return result == comboBox.length;

    }

    /**
     * Method checks data in ui components and sets style;
     * @param datePickers - components which will be checked;
     * @return - if data is correct then return true, else return false;
     */
    public static boolean checkData(DatePicker... datePickers){

        int result = 0;

        for (DatePicker datePicker : datePickers) {
            if (datePicker.getValue() == null){
                setStyle(datePicker, BORDER_RED);
                datePicker.requestFocus();
            }else {
                setStyle(datePicker, BORDER_GREEN);
                result++;
            }
        }

        return result == datePickers.length;

    }

    /**
     * Method checks data in ui components and sets style;
     * @param checkBoxes - components which will be checked;
     * @return - if data is correct then return true, else return false;
     */
    public static boolean checkData(CheckBox... checkBoxes){

        for (CheckBox checkBox : checkBoxes) {
            if (checkBox.isSelected()){
                setStyle(checkBox, BORDER_GREEN);

                return true;
            }else {
                setStyle(checkBox, BORDER_RED);
                checkBox.requestFocus();
            }
        }

        return false;
    }

    /**
     * Method checks data in ui components and sets style;
     * @param radioButtons - components which will be checked;
     * @return - if data is correct then return true, else return false;
     */
    public static boolean checkData(RadioButton... radioButtons){

        for (RadioButton radioButton : radioButtons) {
            if (radioButton.isSelected()){
                setStyle(radioButton, BORDER_GREEN);
                return true;
            }else {
                setStyle(radioButton, BORDER_RED);
                radioButton.requestFocus();
            }
        }

        return false;
    }

    /**
     * Method sets style in component node;
     * @param node - components which will be set style;
     * @param style - style which will be used;
     */
    private static void setStyle(Node node, String style){
        node.setStyle(style);
    }
}
