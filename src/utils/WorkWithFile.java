package utils;

import java.awt.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * the class has methods for work with files(e.g. creating, opening etc.);
 */
public final class WorkWithFile {
    /**
     * the method creates file and returns it;
     * @param URL - path file;
     * @return created file;
     */
    public static File createNewFile(String URL){
        File file = new File(URL);

        try {
            file.createNewFile();
        } catch (IOException e) {
            return null;
        }
        return file;
    }

    /**
     * the method opens the file;
     * @param file which will be opened;
     */
    public static void openTextFile(File file) {
        try {
            Desktop.getDesktop().open(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * the method writes to file text;
     * @param file
     * @param text which will be written to the file
     */
    public static void writeToFile(File file, String text){
        FileWriter fileWriter;
        try {
            fileWriter = new FileWriter(file);

            fileWriter.write(text);
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
