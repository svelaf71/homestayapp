package objects;

import objects.core.AObject;

/**
 * Areas
 */
public class Area extends AObject{
    /**
     * Constructor with param
     * @param name - name of Area (like SELANGOR)
     */
    public Area(String name) {
        super(name);
    }

    /**
     * Default constructor
     */
    public Area() {
    }
}
