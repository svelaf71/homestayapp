package objects;

import javafx.beans.property.SimpleIntegerProperty;
import objects.core.AObject;

/**
 * Class the price values
 */
public class Price extends AObject{

    private SimpleIntegerProperty price = new SimpleIntegerProperty();

    public Price(String name, int price) {
        super(name);
        this.price.set(price);
    }

    public Price() {
    }

    public int getPrice() {
        return price.get();
    }

    public SimpleIntegerProperty priceProperty() {
        return price;
    }

    public void setPrice(int price) {
        this.price.set(price);
    }
}
