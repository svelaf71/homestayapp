package objects;

import javafx.beans.property.SimpleStringProperty;

/**
 * Class for Paypal's data (required in FourthForm)
 */
public class Paypal {

    private SimpleStringProperty email = new SimpleStringProperty("");;

    public Paypal(String email) {
        this.email.set(email);
    }

    public String getEmail() {
        return email.get();
    }

    public SimpleStringProperty emailProperty() {
        return email;
    }

    public void setEmail(String email) {
        this.email.set(email);
    }
}
