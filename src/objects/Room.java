package objects;

import objects.core.AObject;

/**
 * Class for rooms' number
 */
public class Room extends AObject {
    public Room(String name) {
        super(name);
    }

    public Room() {
    }
}
