package objects;

import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;

/**
 * Class for Houses;
 */
public class House{

    /**
     * House's attributes:
     * - Name;
     * - Facilities (Wi-Fi, BBQ...);
     * - Number of Rooms;
     * - Price;
     * - Area;
     * - Availability;
     * - Image;
     */
    private SimpleStringProperty name = new SimpleStringProperty("");
    private SimpleListProperty<Facility> facilities = new SimpleListProperty<Facility>();
    private SimpleObjectProperty<Room> room = new SimpleObjectProperty<>();
    private SimpleObjectProperty<Price> price = new SimpleObjectProperty<>();
    private SimpleObjectProperty<Area> area = new SimpleObjectProperty<>();
    private SimpleListProperty<Month> availability = new SimpleListProperty<>();
    private SimpleObjectProperty<Image> image = new SimpleObjectProperty<>();

    public House(String name, ObservableList<Facility> facilities,
                 Room room, Price price, Area area,
                 ObservableList<Month> availability, Image image) {
        this.name.set(name);
        this.facilities.set(facilities);
        this.room.set(room);
        this.price.set(price);
        this.area.set(area);
        this.availability.set(availability);
        this.image.set(image);
    }

    public House() {
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public ObservableList<Facility> getFacilities() {
        return facilities.get();
    }

    public SimpleListProperty<Facility> facilitiesProperty() {
        return facilities;
    }

    public void setFacilities(ObservableList<Facility> facilities) {
        this.facilities.set(facilities);
    }

    public Room getRoom() {
        return room.get();
    }

    public SimpleObjectProperty<Room> roomProperty() {
        return room;
    }

    public void setRoom(Room room) {
        this.room.set(room);
    }

    public Price getPrice() {
        return price.get();
    }

    public SimpleObjectProperty<Price> priceProperty() {
        return price;
    }

    public void setPrice(Price price) {
        this.price.set(price);
    }

    public Area getArea() {
        return area.get();
    }

    public SimpleObjectProperty<Area> areaProperty() {
        return area;
    }

    public void setArea(Area area) {
        this.area.set(area);
    }

    public ObservableList<Month> getAvailability() {
        return availability.get();
    }

    public SimpleListProperty<Month> availabilityProperty() {
        return availability;
    }

    public void setAvailability(ObservableList<Month> availability) {
        this.availability.set(availability);
    }

    public Image getImage() {
        return image.get();
    }

    public SimpleObjectProperty<Image> imageProperty() {
        return image;
    }

    public void setImage(Image image) {
        this.image.set(image);
    }

    @Override
    public String toString() {
        return name.getName();
    }

}
