package objects.core;

import javafx.beans.property.SimpleStringProperty;

/**
 * abstract class that implements param name and override toString()-method
 */
public abstract class AObject implements IObject {
    private SimpleStringProperty name;

    public AObject(String name) {
        this.name = new SimpleStringProperty(name);
    }

    public AObject() {
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    @Override
    public String toString() {
        return getName();
    }
}
