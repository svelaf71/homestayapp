package objects.core;

import javafx.beans.property.SimpleStringProperty;

/**
 * interface for classes with one param (name)
 */
public interface IObject {

    String getName();

    SimpleStringProperty nameProperty();

    void setName(String name);
}
