package objects;

import javafx.beans.property.SimpleStringProperty;

/**
 * Class for CreditCard's data (required in FourthForm)
 */
public class CreditCard {

    private SimpleStringProperty cardType = new SimpleStringProperty("");
    private SimpleStringProperty cardNo = new SimpleStringProperty("");
    private SimpleStringProperty cvc = new SimpleStringProperty("");
    private SimpleStringProperty expiry = new SimpleStringProperty("");

    public CreditCard(String cardType, String cardNo, String cvc, String expiry) {
        this.cardType.set(cardType);
        this.cardNo.set(cardNo);
        this.cvc.set(cvc);
        this.expiry.set(expiry);
    }

    public String getCardType() {
        return cardType.get();
    }

    public SimpleStringProperty cardTypeProperty() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType.set(cardType);
    }

    public String getCardNo() {
        return cardNo.get();
    }

    public SimpleStringProperty cardNoProperty() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo.set(cardNo);
    }

    public String getCvc() {
        return cvc.get();
    }

    public SimpleStringProperty cvcProperty() {
        return cvc;
    }

    public void setCvc(String cvc) {
        this.cvc.set(cvc);
    }

    public String getExpiry() {
        return expiry.get();
    }

    public SimpleStringProperty expiryProperty() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry.set(expiry);
    }
}
