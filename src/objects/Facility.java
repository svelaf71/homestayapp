package objects;

import objects.core.AObject;

/**
 * Class for Facilities
 */
public class Facility extends AObject {
    public Facility(String name) {
        super(name);
    }

    public Facility() {
    }
}
