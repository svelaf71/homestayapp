package objects;

import javafx.beans.property.SimpleIntegerProperty;
import objects.core.AObject;

/**
 * Class for the price range for different houses
 */
public class PriceRange extends AObject {

    private SimpleIntegerProperty minPrice = new SimpleIntegerProperty();
    private SimpleIntegerProperty maxPrice = new SimpleIntegerProperty();

    public PriceRange(String name) {
        super(name);
    }

    public PriceRange() {
    }

    public PriceRange(String name, int minPrice,
                      int maxPrice) {
        super(name);
        this.minPrice.set(minPrice);
        this.maxPrice.set(maxPrice);
    }

    public int getMinPrice() {
        return minPrice.get();
    }

    public SimpleIntegerProperty minPriceProperty() {
        return minPrice;
    }

    public void setMinPrice(int minPrice) {
        this.minPrice.set(minPrice);
    }

    public int getMaxPrice() {
        return maxPrice.get();
    }

    public SimpleIntegerProperty maxPriceProperty() {
        return maxPrice;
    }

    public void setMaxPrice(int maxPrice) {
        this.maxPrice.set(maxPrice);
    }

    @Override
    public String toString() {
        if (minPrice.get() == 0 && maxPrice.get() == 0){
            return getName();
        }

        return getName() + minPrice.get() +
                "-" + maxPrice.get();
    }
}
