package objects;

import objects.core.AObject;

/**
 * Class for Countries
 */
public class Country extends AObject {
    public Country(String name) {
        super(name);
    }

    public Country() {
    }
}
