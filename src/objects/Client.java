package objects;

import javafx.beans.property.SimpleStringProperty;

/**
 * Class that is responsible for all the client's data (required in ThirdForm)
 */
public class Client {

    /**
     * Client's attributes:
     * - Full Name (firstName, middleName, lastName);
     * - Phone Number (phoneNumber);
     * - Email Address (emailAddress);
     * - IC/Passport Number (passportNumber);
     * - Address (addressLine1, addressLine2);
     * - City (city);
     * - Country (country);
     */
    private SimpleStringProperty firstName = new SimpleStringProperty();
    private SimpleStringProperty middleName = new SimpleStringProperty();
    private SimpleStringProperty lastName = new SimpleStringProperty();
    private SimpleStringProperty phoneNumber = new SimpleStringProperty();
    private SimpleStringProperty emailAddress = new SimpleStringProperty();
    private SimpleStringProperty passportNumber = new SimpleStringProperty();
    private SimpleStringProperty addressLine1 = new SimpleStringProperty();
    private SimpleStringProperty addressLine2 = new SimpleStringProperty();
    private SimpleStringProperty city = new SimpleStringProperty();
    private SimpleStringProperty country = new SimpleStringProperty();

    public Client(String firstName, String middleName, String lastName, String phoneNumber, String emailAddress,
                  String passportNumber, String addressLine1, String addressLine2, String city, String country) {
        this.firstName.set(firstName);
        this.middleName.set(middleName);
        this.lastName.set(lastName);
        this.phoneNumber.set(phoneNumber);
        this.emailAddress.set(emailAddress);
        this.passportNumber.set(passportNumber);
        this.addressLine1.set(addressLine1);
        this.addressLine2.set(addressLine2);
        this.city.set(city);
        this.country.set(country);
    }

    public Client() {}

    public Client(String phoneNumber, String emailAddress, String addressLine1, String city, String country) {
        this.phoneNumber.set(phoneNumber);
        this.emailAddress.set(emailAddress);
        this.addressLine1.set(addressLine1);
        this.city.set(city);
        this.country.set(country);
    }

    public String getFirstName() {
        return firstName.get();
    }

    public SimpleStringProperty firstNameProperty() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName.set(firstName);
    }

    public String getMiddleName() {
        return middleName.get();
    }

    public SimpleStringProperty middleNameProperty() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName.set(middleName);
    }

    public String getLastName() {
        return lastName.get();
    }

    public SimpleStringProperty lastNameProperty() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName.set(lastName);
    }

    public String getPhoneNumber() {
        return phoneNumber.get();
    }

    public SimpleStringProperty phoneNumberProperty() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber.set(phoneNumber);
    }

    public String getEmailAddress() {
        return emailAddress.get();
    }

    public SimpleStringProperty emailAddressProperty() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress.set(emailAddress);
    }

    public String getAddressLine1() {
        return addressLine1.get();
    }

    public SimpleStringProperty addressLine1Property() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1.set(addressLine1);
    }

    public String getAddressLine2() {
        return addressLine2.get();
    }

    public SimpleStringProperty addressLine2Property() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2.set(addressLine2);
    }

    public String getCity() {
        return city.get();
    }

    public SimpleStringProperty cityProperty() {
        return city;
    }

    public void setCity(String city) {
        this.city.set(city);
    }

    public String getCountry() {
        return country.get();
    }

    public SimpleStringProperty countryProperty() {
        return country;
    }

    public void setCountry(String country) {
        this.country.set(country);
    }

    public String getPassportNumber() {
        return passportNumber.get();
    }

    public SimpleStringProperty passportNumberProperty() {
        return passportNumber;
    }
}
