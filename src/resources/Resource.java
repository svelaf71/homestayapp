package resources;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import objects.*;

/**
 * class contains other classes with the default information (like numbers of rooms, names of areas, etc.)
 *
 * it was called Resource cause it contains all basic information and allows to change it easily without
 * surfing all the code :)
 */
public class Resource {

    /**
     * class with areas
     */
    public static final class AreaCollection{
        static final Area SELANGOR = new Area("Selangor");
        static final Area PERAK = new Area("Perak");
        static final Area MELAKA = new Area("Melaka");

        public static final ObservableList<Area> AREAS =
                FXCollections.observableArrayList(SELANGOR, PERAK, MELAKA);
    }

    /**
     * class with price ranges
     */
    public static final class PriceRangeCollection{
        static final PriceRange MYR300_MYR600 = new PriceRange("MYR", 300, 600);
        static final PriceRange MYR601_MYR800 = new PriceRange("MYR", 601, 800);
        static final PriceRange MYR801_MYR1000 = new PriceRange("MYR", 801, 1000);
        static final PriceRange MYR1001_MYR1300 = new PriceRange("MYR", 1001, 1300);
        public static final PriceRange ALL = new PriceRange("All");

        public static final ObservableList<PriceRange> PRICES =
                FXCollections.observableArrayList(MYR300_MYR600, MYR601_MYR800,
                        MYR801_MYR1000, MYR1001_MYR1300, ALL);
    }

    /**
     * class with number of rooms
     */
    public static final class RoomsCollection {
        static final Room ONE = new Room("1");
        static final Room TWO = new Room("2");
        static final Room THREE = new Room("3");
        static final Room FOUR = new Room("4");
        public static final Room ALL = new Room("All");

        public static final ObservableList<Room> ROOMS =
                FXCollections.observableArrayList(ONE, TWO, THREE, FOUR, ALL);
    }

    /**
     * class with list of facilities
     */
    public static final class FacilitiesCollection{
        public static final Facility WIFI = new Facility("Wi-Fi");
        public static final Facility AIR_CONDITION = new Facility("Air-Condition");
        public static final Facility TV_CABLE = new Facility("TV with Cable");
        public static final Facility RENTAL_CARS = new Facility("Rental Cars");
        public static final Facility SWIMMING_POOL = new Facility("Swimming Pool");
        public static final Facility BBQ = new Facility("BBQ");

        public static final ObservableList<Facility> FACILITIES =
                FXCollections.observableArrayList(WIFI, AIR_CONDITION,
                        TV_CABLE, RENTAL_CARS, SWIMMING_POOL, BBQ);
    }

    /**
     * class with images URL-s and houses' data
     */
    public static final class HousesCollection{
        private static final String URL_IMAGE_MELAKA = "@../../resources/images/houses/Melaka/";
        private static final String URL_IMAGE_PERAK = "@../../resources/images/houses/Perak/";
        private static final String URL_IMAGE_SELANGOR = "@../../resources/images/houses/Selangor/";

        private static final String URL_IMAGE_HOUSE_1 = "Apple Tree Cottage.PNG";
        private static final String URL_IMAGE_HOUSE_2 = "Childer Stone Villa.PNG";
        private static final String URL_IMAGE_HOUSE_3 = "Cherry Point Cottage.PNG";
        private static final String URL_IMAGE_HOUSE_4 = "Diamond Hill Cottage.PNG";
        private static final String URL_IMAGE_HOUSE_5 = "Elm Field Villa.PNG";
        private static final String URL_IMAGE_HOUSE_6 = "Forge Cottage.PNG";
        private static final String URL_IMAGE_HOUSE_7 = "Orchard Hill Cottage.PNG";
        private static final String URL_IMAGE_HOUSE_8 = "Holly Cottage.PNG";
        private static final String URL_IMAGE_HOUSE_9 = "Ivy Terrace Villa.PNG";
        private static final String URL_IMAGE_HOUSE_10 = "Knights Wood Villa.PNG";
        private static final String URL_IMAGE_HOUSE_11 = "Palm Ocean Cottage.PNG";
        private static final String URL_IMAGE_HOUSE_12 = "Little Hollow Cottage.PNG";
        private static final String URL_IMAGE_HOUSE_13 = "Lindisfarne Manor.PNG";

        private static final String URL_IMAGE_HOUSE_P_1 = "Lumsdens Villa.PNG";
        private static final String URL_IMAGE_HOUSE_P_2 = "Lush Resort.PNG";
        private static final String URL_IMAGE_HOUSE_P_3 = "The Keep Cottage.PNG";
        private static final String URL_IMAGE_HOUSE_P_4 = "Jungle Woods Cottage.PNG";
        private static final String URL_IMAGE_HOUSE_P_5 = "Valley Hill Resort.PNG";
        private static final String URL_IMAGE_HOUSE_P_6 = "Hillcrest Resort.PNG";
        private static final String URL_IMAGE_HOUSE_P_7 = "Urban Vibe Resort.PNG";
        private static final String URL_IMAGE_HOUSE_P_8 = "Hollyoak Villa.PNG";
        private static final String URL_IMAGE_HOUSE_P_9 = "Kintbury Resort.PNG";
        private static final String URL_IMAGE_HOUSE_P_10 = "Maddle Farm Villa.PNG";
        private static final String URL_IMAGE_HOUSE_P_11 = "Lavendar Hill Resort.PNG";
        private static final String URL_IMAGE_HOUSE_P_12 = "Thornton Valley Cottage.PNG";
        private static final String URL_IMAGE_HOUSE_P_13 = "Foxcote Villa.PNG";

        private static final String URL_IMAGE_HOUSE_S_1 = "Appleshow Holiday Resort.PNG";
        private static final String URL_IMAGE_HOUSE_S_2 = "Ashdon Holiday Villa.PNG";
        private static final String URL_IMAGE_HOUSE_S_3 = "Dryhill Holiday Resort.PNG";
        private static final String URL_IMAGE_HOUSE_S_4 = "Farhampton Inn.PNG";
        private static final String URL_IMAGE_HOUSE_S_5 = "Harlow Cottage.PNG";
        private static final String URL_IMAGE_HOUSE_S_6 = "Langstone Cottage.PNG";
        private static final String URL_IMAGE_HOUSE_S_7 = "Latimer Holiday Resort.PNG";
        private static final String URL_IMAGE_HOUSE_S_8 = "Lavendon Villa.PNG";
        private static final String URL_IMAGE_HOUSE_S_9 = "Moor Park Holiday Resort.PNG";
        private static final String URL_IMAGE_HOUSE_S_10 = "Preston Resort.PNG";
        private static final String URL_IMAGE_HOUSE_S_11 = "Priest Wood Villa.PNG";
        private static final String URL_IMAGE_HOUSE_S_12 = "Radwell Villa.PNG";
        private static final String URL_IMAGE_HOUSE_S_13 = "Wortley Resort.PNG";

        static final House HOUSE_1 = new House("Apple Tree Cottage",
                (FacilitiesCollection.FACILITIES),
                ((Room) RoomsCollection.FOUR),
                (new Price("MYR", 1200)),
                ((Area) AreaCollection.MELAKA),
                FXCollections.observableArrayList(Month.JANUARY, Month.FEBRUARY,
                        Month.MARCH, Month.APRIL, Month.SEPTEMBER, Month.OCTOBER,
                        Month.NOVEMBER, Month.DECEMBER),
                new Image(URL_IMAGE_MELAKA + URL_IMAGE_HOUSE_1));

        static final House HOUSE_2 = new House("Childer Stone Villa",
                (FXCollections.observableArrayList(FacilitiesCollection.WIFI,
                        FacilitiesCollection.AIR_CONDITION, FacilitiesCollection.TV_CABLE,
                        FacilitiesCollection.RENTAL_CARS, FacilitiesCollection.SWIMMING_POOL)),
                ((Room) RoomsCollection.THREE),
                (new Price("MYR", 900)),
                ((Area) AreaCollection.MELAKA),
                FXCollections.observableArrayList(Month.MAY, Month.JULY,
                        Month.JUNE, Month.AUGUST, Month.SEPTEMBER, Month.OCTOBER,
                        Month.NOVEMBER, Month.DECEMBER),
                new Image(URL_IMAGE_MELAKA + URL_IMAGE_HOUSE_2));

        static final House HOUSE_3 = new House("Cherry Point Cottage",
                (FacilitiesCollection.FACILITIES),
                ((Room) RoomsCollection.TWO),
                (new Price("MYR", 700)),
                ((Area) AreaCollection.MELAKA),
                FXCollections.observableArrayList(Month.JANUARY, Month.FEBRUARY,
                        Month.MARCH, Month.APRIL, Month.SEPTEMBER, Month.OCTOBER,
                        Month.NOVEMBER, Month.DECEMBER),
                new Image(URL_IMAGE_MELAKA + URL_IMAGE_HOUSE_3));

        static final House HOUSE_4 = new House("Diamond Hill Cottage",
                (FXCollections.observableArrayList(FacilitiesCollection.WIFI,
                        FacilitiesCollection.AIR_CONDITION, FacilitiesCollection.TV_CABLE,
                        FacilitiesCollection.BBQ)),
                ((Room) RoomsCollection.ONE),
                (new Price("MYR", 500)),
                ((Area) AreaCollection.MELAKA),
                FXCollections.observableArrayList(Month.MAY, Month.JULY,
                        Month.JUNE, Month.AUGUST, Month.SEPTEMBER, Month.OCTOBER,
                        Month.NOVEMBER, Month.DECEMBER),
                new Image(URL_IMAGE_MELAKA + URL_IMAGE_HOUSE_4));

        static final House HOUSE_5 = new House("Elm Field Villa",
                (FXCollections.observableArrayList(FacilitiesCollection.WIFI,
                        FacilitiesCollection.AIR_CONDITION,
                        FacilitiesCollection.TV_CABLE)),
                ((Room) RoomsCollection.ONE),
                (new Price("MYR", 300)),
                ((Area) AreaCollection.MELAKA),
                FXCollections.observableArrayList(Month.MAY, Month.JULY,
                        Month.JUNE, Month.AUGUST, Month.SEPTEMBER, Month.OCTOBER,
                        Month.NOVEMBER, Month.DECEMBER),
                new Image(URL_IMAGE_MELAKA + URL_IMAGE_HOUSE_5));

        static final House HOUSE_6 = new House("Forge Cottage",
                (FXCollections.observableArrayList(FacilitiesCollection.WIFI,
                        FacilitiesCollection.AIR_CONDITION,
                        FacilitiesCollection.TV_CABLE, FacilitiesCollection.BBQ)),
                ((Room) RoomsCollection.THREE),
                (new Price("MYR", 750)),
                ((Area) AreaCollection.MELAKA),
                FXCollections.observableArrayList(Month.JANUARY, Month.FEBRUARY,
                        Month.MARCH, Month.APRIL, Month.SEPTEMBER, Month.OCTOBER,
                        Month.NOVEMBER, Month.DECEMBER),
                new Image(URL_IMAGE_MELAKA + URL_IMAGE_HOUSE_6));

        static final House HOUSE_7 = new House("Orchard Hill Cottage",
                (FacilitiesCollection.FACILITIES),
                ((Room) RoomsCollection.FOUR),
                (new Price("MYR", 950)),
                ((Area) AreaCollection.MELAKA),
                FXCollections.observableArrayList(Month.MAY, Month.JULY,
                        Month.JUNE, Month.AUGUST, Month.SEPTEMBER, Month.OCTOBER,
                        Month.NOVEMBER, Month.DECEMBER),
                new Image(URL_IMAGE_MELAKA + URL_IMAGE_HOUSE_7));

        static final House HOUSE_8 = new House("Holly Cottage",
                (FacilitiesCollection.FACILITIES),
                ((Room) RoomsCollection.TWO),
                (new Price("MYR", 800)),
                ((Area) AreaCollection.MELAKA),
                FXCollections.observableArrayList(Month.JANUARY,
                        Month.FEBRUARY, Month.MARCH, Month.APRIL,
                        Month.MAY, Month.JULY,
                        Month.JUNE, Month.AUGUST),
                new Image(URL_IMAGE_MELAKA + URL_IMAGE_HOUSE_8));

        static final House HOUSE_9 = new House("Ivy Terrace Villa",
                (FacilitiesCollection.FACILITIES),
                ((Room) RoomsCollection.THREE),
                (new Price("MYR", 1000)),
                ((Area) AreaCollection.MELAKA),
                FXCollections.observableArrayList(Month.JANUARY, Month.FEBRUARY,
                        Month.MARCH, Month.APRIL, Month.SEPTEMBER, Month.OCTOBER,
                        Month.NOVEMBER, Month.DECEMBER),
                new Image(URL_IMAGE_MELAKA + URL_IMAGE_HOUSE_9));

        static final House HOUSE_10 = new House("Knights Wood Villa",
                (FacilitiesCollection.FACILITIES),
                ((Room) RoomsCollection.TWO),
                (new Price("MYR", 450)),
                ((Area) AreaCollection.MELAKA),
                FXCollections.observableArrayList(Month.JANUARY, Month.FEBRUARY,
                        Month.MARCH, Month.APRIL, Month.SEPTEMBER, Month.OCTOBER,
                        Month.NOVEMBER, Month.DECEMBER),
                new Image(URL_IMAGE_MELAKA + URL_IMAGE_HOUSE_10));

        static final House HOUSE_11 = new House("Palm Ocean Cottage",
                (FacilitiesCollection.FACILITIES),
                ((Room) RoomsCollection.FOUR),
                (new Price("MYR", 1200)),
                ((Area) AreaCollection.MELAKA),
                FXCollections.observableArrayList(Month.JANUARY,
                        Month.FEBRUARY, Month.MARCH, Month.APRIL,
                        Month.MAY, Month.JULY,
                        Month.JUNE, Month.AUGUST),
                new Image(URL_IMAGE_MELAKA + URL_IMAGE_HOUSE_11));

        static final House HOUSE_12 = new House("Little Hollow Cottage",
                (FXCollections.observableArrayList(FacilitiesCollection.WIFI,
                        FacilitiesCollection.AIR_CONDITION,
                        FacilitiesCollection.TV_CABLE,
                        FacilitiesCollection.RENTAL_CARS,
                        FacilitiesCollection.BBQ)),
                ((Room) RoomsCollection.TWO),
                (new Price("MYR", 700)),
                ((Area) AreaCollection.MELAKA),
                FXCollections.observableArrayList(Month.JANUARY,
                        Month.FEBRUARY, Month.MARCH, Month.APRIL,
                        Month.MAY, Month.JULY,
                        Month.JUNE, Month.AUGUST),
                new Image(URL_IMAGE_MELAKA + URL_IMAGE_HOUSE_12));

        static final House HOUSE_13 = new House("Lindisfarne Manor",
                (FacilitiesCollection.FACILITIES),
                ((Room) RoomsCollection.THREE),
                (new Price("MYR", 900)),
                ((Area) AreaCollection.MELAKA),
                FXCollections.observableArrayList(Month.JANUARY,
                        Month.FEBRUARY, Month.MARCH, Month.APRIL,
                        Month.SEPTEMBER, Month.OCTOBER,
                        Month.NOVEMBER, Month.DECEMBER),
                new Image(URL_IMAGE_MELAKA + URL_IMAGE_HOUSE_13));

        static final House HOUSE_P_1 = new House("Lumsdens Villa",
                (FXCollections.observableArrayList(FacilitiesCollection.WIFI,
                        FacilitiesCollection.AIR_CONDITION,
                        FacilitiesCollection.TV_CABLE,
                        FacilitiesCollection.RENTAL_CARS,
                        FacilitiesCollection.SWIMMING_POOL)),
                ((Room) RoomsCollection.THREE),
                (new Price("MYR", 950)),
                ((Area) AreaCollection.PERAK),
                FXCollections.observableArrayList(Month.JANUARY,
                        Month.FEBRUARY, Month.MARCH, Month.APRIL,
                        Month.SEPTEMBER, Month.OCTOBER,
                        Month.NOVEMBER, Month.DECEMBER),
                new Image(URL_IMAGE_PERAK+ URL_IMAGE_HOUSE_P_1));

        static final House HOUSE_P_2 = new House("Lush Resort",
                (FacilitiesCollection.FACILITIES),
                ((Room) RoomsCollection.FOUR),
                (new Price("MYR", 1200)),
                ((Area) AreaCollection.PERAK),
                FXCollections.observableArrayList(Month.JANUARY,
                        Month.FEBRUARY, Month.MARCH, Month.APRIL,
                        Month.SEPTEMBER, Month.OCTOBER,
                        Month.NOVEMBER, Month.DECEMBER),
                new Image(URL_IMAGE_PERAK+ URL_IMAGE_HOUSE_P_2));

        static final House HOUSE_P_3 = new House("The Keep Cottage",
                (FacilitiesCollection.FACILITIES),
                ((Room) RoomsCollection.THREE),
                (new Price("MYR", 1000)),
                ((Area) AreaCollection.PERAK),
                FXCollections.observableArrayList(Month.JANUARY,
                        Month.FEBRUARY, Month.MARCH, Month.APRIL,
                        Month.MAY, Month.JUNE, Month.JULY, Month.AUGUST,
                        Month.SEPTEMBER, Month.OCTOBER,
                        Month.NOVEMBER, Month.DECEMBER),
                new Image(URL_IMAGE_PERAK+ URL_IMAGE_HOUSE_P_3));

        static final House HOUSE_P_4 = new House("Jungle Woods Cottage",
                (FacilitiesCollection.FACILITIES),
                ((Room) RoomsCollection.FOUR),
                (new Price("MYR", 1100)),
                ((Area) AreaCollection.PERAK),
                FXCollections.observableArrayList(Month.JANUARY,
                        Month.FEBRUARY, Month.MARCH, Month.APRIL,
                        Month.MAY, Month.JUNE, Month.JULY, Month.AUGUST,
                        Month.SEPTEMBER, Month.OCTOBER,
                        Month.NOVEMBER, Month.DECEMBER),
                new Image(URL_IMAGE_PERAK+ URL_IMAGE_HOUSE_P_4));

        static final House HOUSE_P_5 = new House("Valley Hill Resort",
                (FacilitiesCollection.FACILITIES),
                ((Room) RoomsCollection.FOUR),
                (new Price("MYR", 1200)),
                ((Area) AreaCollection.PERAK),
                FXCollections.observableArrayList(Month.JANUARY,
                        Month.FEBRUARY, Month.MARCH, Month.APRIL,
                        Month.MAY, Month.JUNE, Month.JULY, Month.AUGUST),
                new Image(URL_IMAGE_PERAK+ URL_IMAGE_HOUSE_P_5));

        static final House HOUSE_P_6 = new House("Hillcrest Resort",
                (FacilitiesCollection.FACILITIES),
                ((Room) RoomsCollection.THREE),
                (new Price("MYR", 1200)),
                ((Area) AreaCollection.PERAK),
                FXCollections.observableArrayList(Month.JANUARY,
                        Month.FEBRUARY, Month.MARCH, Month.APRIL,
                        Month.SEPTEMBER, Month.OCTOBER,
                        Month.NOVEMBER, Month.DECEMBER),
                new Image(URL_IMAGE_PERAK+ URL_IMAGE_HOUSE_P_6));

        static final House HOUSE_P_7 = new House("Urban Vibe Resort",
                (FXCollections.observableArrayList(FacilitiesCollection.WIFI,
                        FacilitiesCollection.AIR_CONDITION,
                        FacilitiesCollection.TV_CABLE,
                        FacilitiesCollection.RENTAL_CARS,
                        FacilitiesCollection.BBQ)),
                ((Room) RoomsCollection.FOUR),
                (new Price("MYR", 1000)),
                ((Area) AreaCollection.PERAK),
                FXCollections.observableArrayList(Month.JANUARY,
                        Month.FEBRUARY, Month.MARCH, Month.APRIL,
                        Month.MAY, Month.JUNE, Month.JULY, Month.AUGUST,
                        Month.SEPTEMBER, Month.OCTOBER,
                        Month.NOVEMBER, Month.DECEMBER),
                new Image(URL_IMAGE_PERAK+ URL_IMAGE_HOUSE_P_7));

        static final House HOUSE_P_8 = new House("Hollyoak Villa",
                (FXCollections.observableArrayList(FacilitiesCollection.WIFI,
                        FacilitiesCollection.AIR_CONDITION,
                        FacilitiesCollection.TV_CABLE,
                        FacilitiesCollection.BBQ)),
                ((Room) RoomsCollection.THREE),
                (new Price("MYR", 950)),
                ((Area) AreaCollection.PERAK),
                FXCollections.observableArrayList(Month.JANUARY,
                        Month.FEBRUARY, Month.MARCH, Month.APRIL,
                        Month.MAY, Month.JUNE, Month.JULY, Month.AUGUST,
                        Month.SEPTEMBER, Month.OCTOBER,
                        Month.NOVEMBER, Month.DECEMBER),
                new Image(URL_IMAGE_PERAK+ URL_IMAGE_HOUSE_P_8));

        static final House HOUSE_P_9 = new House("Kintbury Resort",
                (FXCollections.observableArrayList(FacilitiesCollection.WIFI,
                        FacilitiesCollection.AIR_CONDITION,
                        FacilitiesCollection.TV_CABLE)),
                ((Room) RoomsCollection.TWO),
                (new Price("MYR", 500)),
                ((Area) AreaCollection.PERAK),
                FXCollections.observableArrayList(Month.MAY,
                        Month.JUNE, Month.JULY, Month.AUGUST,
                        Month.SEPTEMBER, Month.OCTOBER,
                        Month.NOVEMBER, Month.DECEMBER),
                new Image(URL_IMAGE_PERAK+ URL_IMAGE_HOUSE_P_9));

        static final House HOUSE_P_10 = new House("Maddle Farm Villa",
                (FXCollections.observableArrayList(FacilitiesCollection.WIFI,
                        FacilitiesCollection.AIR_CONDITION,
                        FacilitiesCollection.TV_CABLE)),
                ((Room) RoomsCollection.ONE),
                (new Price("MYR", 400)),
                ((Area) AreaCollection.PERAK),
                FXCollections.observableArrayList(Month.JANUARY,
                        Month.FEBRUARY, Month.MARCH, Month.APRIL,
                        Month.SEPTEMBER, Month.OCTOBER,
                        Month.NOVEMBER, Month.DECEMBER),
                new Image(URL_IMAGE_PERAK+ URL_IMAGE_HOUSE_P_10));

        static final House HOUSE_P_11 = new House("Lavendar Hill Resort",
                (FXCollections.observableArrayList(FacilitiesCollection.WIFI,
                        FacilitiesCollection.AIR_CONDITION,
                        FacilitiesCollection.BBQ)),
                ((Room) RoomsCollection.TWO),
                (new Price("MYR", 600)),
                ((Area) AreaCollection.PERAK),
                FXCollections.observableArrayList(Month.MAY,
                        Month.JUNE, Month.JULY, Month.AUGUST,
                        Month.SEPTEMBER, Month.OCTOBER,
                        Month.NOVEMBER, Month.DECEMBER),
                new Image(URL_IMAGE_PERAK+ URL_IMAGE_HOUSE_P_11));

        static final House HOUSE_P_12 = new House("Thornton Valley Cottage",
                (FacilitiesCollection.FACILITIES),
                ((Room) RoomsCollection.THREE),
                (new Price("MYR", 850)),
                ((Area) AreaCollection.PERAK),
                FXCollections.observableArrayList(Month.JANUARY,
                        Month.FEBRUARY, Month.MARCH, Month.APRIL,
                        Month.SEPTEMBER, Month.NOVEMBER, Month.OCTOBER,
                        Month.DECEMBER),
                new Image(URL_IMAGE_PERAK+ URL_IMAGE_HOUSE_P_12));

        static final House HOUSE_P_13 = new House("Foxcote Villa",
                (FacilitiesCollection.FACILITIES),
                ((Room) RoomsCollection.THREE),
                (new Price("MYR", 110)),
                ((Area) AreaCollection.PERAK),
                FXCollections.observableArrayList(Month.JANUARY,
                        Month.FEBRUARY, Month.MARCH, Month.APRIL,
                        Month.SEPTEMBER, Month.NOVEMBER, Month.OCTOBER,
                        Month.DECEMBER),
                new Image(URL_IMAGE_PERAK+ URL_IMAGE_HOUSE_P_13));

        static final House HOUSE_S_1 = new House("Appleshow Holiday Resort",
                (FacilitiesCollection.FACILITIES),
                ((Room) RoomsCollection.FOUR),
                (new Price("MYR", 1150)),
                ((Area) AreaCollection.SELANGOR),
                FXCollections.observableArrayList(Month.JANUARY,
                        Month.FEBRUARY, Month.MARCH, Month.APRIL,
                        Month.MAY, Month.JUNE, Month.JULY, Month.AUGUST,
                        Month.SEPTEMBER, Month.NOVEMBER, Month.OCTOBER,
                        Month.DECEMBER),
                new Image(URL_IMAGE_SELANGOR + URL_IMAGE_HOUSE_S_1));

        static final House HOUSE_S_2 = new House("Ashdon Holiday Villa",
                (FXCollections.observableArrayList(FacilitiesCollection.WIFI,
                        FacilitiesCollection.AIR_CONDITION,
                        FacilitiesCollection.TV_CABLE)),
                ((Room) RoomsCollection.TWO),
                (new Price("MYR", 500)),
                ((Area) AreaCollection.SELANGOR),
                FXCollections.observableArrayList(Month.MAY, Month.JUNE, Month.JULY,
                        Month.AUGUST, Month.SEPTEMBER, Month.NOVEMBER,
                        Month.OCTOBER, Month.DECEMBER),
                new Image(URL_IMAGE_SELANGOR + URL_IMAGE_HOUSE_S_2));

        static final House HOUSE_S_7 = new House("Latimer Holiday Resort",
                (FacilitiesCollection.FACILITIES),
                ((Room) RoomsCollection.FOUR),
                (new Price("MYR", 1200)),
                ((Area) AreaCollection.SELANGOR),
                FXCollections.observableArrayList(Month.JANUARY,
                        Month.FEBRUARY, Month.MARCH, Month.APRIL,
                        Month.MAY, Month.JUNE, Month.JULY, Month.AUGUST,
                        Month.SEPTEMBER, Month.NOVEMBER, Month.OCTOBER,
                        Month.DECEMBER),
                new Image(URL_IMAGE_SELANGOR + URL_IMAGE_HOUSE_S_7));

        static final House HOUSE_S_8 = new House("Lavendon Villa",
                (FXCollections.observableArrayList(FacilitiesCollection.WIFI,
                        FacilitiesCollection.AIR_CONDITION,
                        FacilitiesCollection.TV_CABLE,
                        FacilitiesCollection.RENTAL_CARS,
                        FacilitiesCollection.SWIMMING_POOL)),
                ((Room) RoomsCollection.TWO),
                (new Price("MYR", 500)),
                ((Area) AreaCollection.SELANGOR),
                FXCollections.observableArrayList(Month.MAY, Month.JUNE, Month.JULY,
                        Month.AUGUST, Month.SEPTEMBER, Month.NOVEMBER,
                        Month.OCTOBER, Month.DECEMBER),
                new Image(URL_IMAGE_SELANGOR + URL_IMAGE_HOUSE_S_8));

        static final House HOUSE_S_4 = new House("Farhampton Inn",
                (FXCollections.observableArrayList(FacilitiesCollection.WIFI,
                        FacilitiesCollection.AIR_CONDITION,
                        FacilitiesCollection.TV_CABLE,
                        FacilitiesCollection.BBQ)),
                ((Room) RoomsCollection.TWO),
                (new Price("MYR", 700)),
                ((Area) AreaCollection.SELANGOR),
                FXCollections.observableArrayList(Month.JANUARY,
                        Month.FEBRUARY, Month.MARCH, Month.APRIL,
                        Month.MAY, Month.JUNE, Month.JULY, Month.AUGUST,
                        Month.SEPTEMBER, Month.NOVEMBER, Month.OCTOBER,
                        Month.DECEMBER),
                new Image(URL_IMAGE_SELANGOR + URL_IMAGE_HOUSE_S_4));

        static final House HOUSE_S_10 = new House("Preston Resort",
                (FacilitiesCollection.FACILITIES),
                ((Room) RoomsCollection.FOUR),
                (new Price("MYR", 1100)),
                ((Area) AreaCollection.SELANGOR),
                FXCollections.observableArrayList(Month.JANUARY,
                        Month.FEBRUARY, Month.MARCH, Month.APRIL,
                        Month.MAY, Month.JUNE, Month.JULY, Month.AUGUST,
                        Month.SEPTEMBER, Month.NOVEMBER, Month.OCTOBER,
                        Month.DECEMBER),
                new Image(URL_IMAGE_SELANGOR + URL_IMAGE_HOUSE_S_10));

        static final House HOUSE_S_5 = new House("Harlow Cottage",
                (FXCollections.observableArrayList(FacilitiesCollection.WIFI,
                        FacilitiesCollection.AIR_CONDITION,
                        FacilitiesCollection.TV_CABLE,
                        FacilitiesCollection.RENTAL_CARS)),
                ((Room) RoomsCollection.THREE),
                (new Price("MYR", 800)),
                ((Area) AreaCollection.SELANGOR),
                FXCollections.observableArrayList(Month.JANUARY,
                        Month.FEBRUARY, Month.MARCH, Month.APRIL,
                        Month.SEPTEMBER, Month.NOVEMBER, Month.OCTOBER,
                        Month.DECEMBER),
                new Image(URL_IMAGE_SELANGOR + URL_IMAGE_HOUSE_S_5));

        static final House HOUSE_S_3 = new House("Dryhill Holiday Resort",
                (FacilitiesCollection.FACILITIES),
                ((Room) RoomsCollection.FOUR),
                (new Price("MYR", 1150)),
                ((Area) AreaCollection.SELANGOR),
                FXCollections.observableArrayList(Month.JANUARY,
                        Month.FEBRUARY, Month.MARCH, Month.APRIL,
                        Month.MAY, Month.JUNE, Month.JULY, Month.AUGUST,
                        Month.SEPTEMBER, Month.NOVEMBER, Month.OCTOBER,
                        Month.DECEMBER),
                new Image(URL_IMAGE_SELANGOR + URL_IMAGE_HOUSE_S_3));

        static final House HOUSE_S_11 = new House("Priest Wood Villa",
                (FXCollections.observableArrayList(FacilitiesCollection.WIFI,
                        FacilitiesCollection.AIR_CONDITION,
                        FacilitiesCollection.TV_CABLE,
                        FacilitiesCollection.BBQ,
                        FacilitiesCollection.SWIMMING_POOL)),
                ((Room) RoomsCollection.THREE),
                (new Price("MYR", 800)),
                ((Area) AreaCollection.SELANGOR),
                FXCollections.observableArrayList(Month.MAY,
                        Month.JUNE, Month.JULY, Month.AUGUST,
                        Month.SEPTEMBER, Month.NOVEMBER, Month.OCTOBER,
                        Month.DECEMBER),
                new Image(URL_IMAGE_SELANGOR + URL_IMAGE_HOUSE_S_11));

        static final House HOUSE_S_13 = new House("Wortley Resort",
                (FacilitiesCollection.FACILITIES),
                ((Room) RoomsCollection.FOUR),
                (new Price("MYR", 1200)),
                ((Area) AreaCollection.SELANGOR),
                FXCollections.observableArrayList(Month.JANUARY,
                        Month.FEBRUARY, Month.MARCH, Month.APRIL,
                        Month.SEPTEMBER, Month.NOVEMBER, Month.OCTOBER,
                        Month.DECEMBER),
                new Image(URL_IMAGE_SELANGOR + URL_IMAGE_HOUSE_S_13));

        static final House HOUSE_S_6 = new House("Langstone Cottage",
                (FXCollections.observableArrayList(FacilitiesCollection.WIFI,
                        FacilitiesCollection.AIR_CONDITION,
                        FacilitiesCollection.TV_CABLE,
                        FacilitiesCollection.RENTAL_CARS,
                        FacilitiesCollection.BBQ)),
                ((Room) RoomsCollection.THREE),
                (new Price("MYR", 800)),
                ((Area) AreaCollection.SELANGOR),
                FXCollections.observableArrayList(Month.MAY,
                        Month.JUNE, Month.JULY, Month.AUGUST,
                        Month.SEPTEMBER, Month.NOVEMBER, Month.OCTOBER,
                        Month.DECEMBER),
                new Image(URL_IMAGE_SELANGOR + URL_IMAGE_HOUSE_S_6));

        static final House HOUSE_S_12 = new House("Radwell Villa",
                (FXCollections.observableArrayList(FacilitiesCollection.WIFI,
                        FacilitiesCollection.AIR_CONDITION,
                        FacilitiesCollection.TV_CABLE)),
                ((Room) RoomsCollection.TWO),
                (new Price("MYR", 600)),
                ((Area) AreaCollection.SELANGOR),
                FXCollections.observableArrayList(Month.JANUARY, Month.FEBRUARY,
                        Month.MARCH, Month.APRIL, Month.MAY,
                        Month.JUNE, Month.JULY, Month.AUGUST,
                        Month.SEPTEMBER, Month.NOVEMBER, Month.OCTOBER,
                        Month.DECEMBER),
                new Image(URL_IMAGE_SELANGOR + URL_IMAGE_HOUSE_S_12));

        static final House HOUSE_S_9 = new House("Moor Park Holiday Resort",
                (FacilitiesCollection.FACILITIES),
                ((Room) RoomsCollection.FOUR),
                (new Price("MYR", 1200)),
                ((Area) AreaCollection.SELANGOR),
                FXCollections.observableArrayList(Month.JANUARY, Month.FEBRUARY,
                        Month.MARCH, Month.APRIL,
                        Month.SEPTEMBER, Month.NOVEMBER, Month.OCTOBER,
                        Month.DECEMBER),
                new Image(URL_IMAGE_SELANGOR + URL_IMAGE_HOUSE_S_9));

        public static final ObservableList<House> HOUSES =
                FXCollections.observableArrayList(HOUSE_1, HOUSE_2, HOUSE_3,
                        HOUSE_4, HOUSE_5, HOUSE_6, HOUSE_7, HOUSE_8, HOUSE_9,
                        HOUSE_10, HOUSE_11, HOUSE_12, HOUSE_13, HOUSE_P_1, HOUSE_P_2, HOUSE_P_3,
                        HOUSE_P_4, HOUSE_P_5, HOUSE_P_6, HOUSE_P_7, HOUSE_P_8, HOUSE_P_9,
                        HOUSE_P_10, HOUSE_P_11, HOUSE_P_12, HOUSE_P_13, HOUSE_S_1, HOUSE_S_2, HOUSE_S_3,

                        HOUSE_S_4, HOUSE_S_5, HOUSE_S_6, HOUSE_S_7, HOUSE_S_8, HOUSE_S_9, HOUSE_S_10,
                        HOUSE_S_11, HOUSE_S_12, HOUSE_S_13);
    }

    /**
     * class with list of countries
     */
    public static final class CountriesCollection {
        static final Country INDIA = new Country("India");
        static final Country CHINA = new Country("China");
        static final Country AUSTRALIA = new Country("Australia");
        static final Country MALAYSIA = new Country("Malaysia");
        static final Country JAPAN = new Country("Japan");
        static final Country HONG_KONG = new Country("Hong Kong");
        static final Country SINGAPORE = new Country("Singapore");
        static final Country INDONESIA = new Country("Indonesia");
        static final Country CAMBODIA = new Country("Cambodia");
        static final Country SOUTH_KOREA = new Country("South Korea");
        static final Country LAOS = new Country("Laos");
        static final Country NEPAL = new Country("Nepal");
        static final Country PHILLIPINES = new Country("Phillipines");
        static final Country MALDIVES = new Country("Maldives");
        static final Country SAUDI_ARABIA = new Country("Saudi Arabia");

        public static final ObservableList<Country> COUNTRIES =
                FXCollections.observableArrayList(INDIA, CHINA, AUSTRALIA, MALAYSIA, JAPAN, HONG_KONG, SINGAPORE,
                        INDONESIA, CAMBODIA, SOUTH_KOREA, LAOS, NEPAL, PHILLIPINES, MALDIVES, SAUDI_ARABIA);
    }

    /**
     * class with errors types
     */
    public static final class Errors{
        public static final String ERROR = "ERROR";
        public static final String ENTER_DATA = "Enter Data";
        public static final String INFO = "INFORMATION";
        public static final String HOUSE_ISNOT_FOUND = "House isn't found";
    }

    /**
     * class with information types
     */
    public static final class Info{
        public static final String INFO = "Information";
        public static final String RESULT = "Payment Received and Booking Completed!";
    }

    /**
     * class with result text (used in printing data to the .txt)
     */
    public static final class ResultText{
        public static final String FULL_NAME = "Full Name: ";
        public static final String PHONE_NUMBER = "Phone Number: ";
        public static final String EMAIL_ADDRESS = "Email address: ";
        public static final String PASPORT_NUMBER = "IC/Passport Number: ";
        public static final String ADRDRESS = "Address: ";
        public static final String CITY = "City: ";
        public static final String COUNTRY = "Country: ";
        public static final String PAYMENT_METHOD = "Payment Method: ";
        public static final String AMOUNT = "Amount: ";
    }
}
