package ui.controllers.forms;

import javafx.scene.Cursor;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import objects.House;
import ui.WindowsService;
import ui.controllers.core.IController;
import utils.DialogManager;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * class-controller is for the Second Form
 */
public class SecondFormController implements IController {

    /**
     * buttons to the next and previous form;
     */
    public Button btnBack;
    public Button btnNext;

    /**
     * list for displaying houses;
     */
    public ListView<House> listResult;

    /**
     * group for radio buttons;
     */
    private final ToggleGroup group = new ToggleGroup();

    /**
     * selected house;
     */
    private static House selectedHouse;

    /**
     * the first method that will be compiled in this class
     * @param location - is not used;
     * @param resources - is not used;
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        selectedHouse = null;

        onClickButtonBack();
        onClickButtonNext();
        onClickListView();
        setData();
    }

    /**
     * the method is for cleaning selected items when the item was selected
     */
    private void onClickListView() {
        listResult.setOnMouseClicked(event -> listResult.getSelectionModel().clearSelection());

    }

    /**
     * the method switch app to the next form;
     */
    private void onClickButtonNext() {
        btnNext.setOnAction(event -> {
            if (checkValues()){
                WindowsService.setWindowThirdForm();
            }
        });
    }

    /**
     * the method check if all required options was chosen by user
     * @return true if all was chosen correctly, false and Error-message when it was not;
     */
    private boolean checkValues() {
        if (selectedHouse == null){
            DialogManager.showErrorDialog("", "");

            return false;
        }

        return true;
    }

    /**
     * the method sets information in list;
     */
    private void setData() {
        listResult.setItems(FirstFormController.getSelectedHouses());
        listResult.setCellFactory(param -> new HouseCell());
    }

    /**
     * the method returns user to the previous form
     */
    private void onClickButtonBack() {
        btnBack.setOnAction(event -> WindowsService.setWindowFirstForm());
    }

    /**
     * the class sets items to the list;
     */
    private class HouseCell extends ListCell<House> {

        @Override
        public void updateItem(House item, boolean empty) {
            super.updateItem(item, empty);
            ViewHouse house = new ViewHouse();

            if (item != null){

                house.setData();
                house.houseName.setText(item.getName());
                house.rent.setText(item.getPrice().getName() + item.getPrice().getPrice());
                house.rooms.setText(item.getRoom().getName());
                house.imageView.setImage(item.getImage());
                house.radioButton.setToggleGroup(group);
                house.radioButton.setId(item.getName());

                setInputFacilities(house, item);

                house.radioButton.setOnAction(event -> selectedHouse = item);
                setGraphic(house.anchorPane);
            }
        }
    }

    /**
     * the method sets text in item;
     * @param house
     * @param item
     */
    private void setInputFacilities(ViewHouse house, House item) {
        if (item.getFacilities().size() <= 3){
            house.facilities.setText(item.getFacilities().toString());
        }else {
            for (int i = 0; i < item.getFacilities().size(); i++){
                if (i <= 2){
                    if (i == 0){
                        house.facilities.setText("[" + item.getFacilities().get(i).getName());
                    }else {
                        house.facilities.setText(house.facilities.getText() + ", " +
                                item.getFacilities().get(i).getName());
                    }
                    if (i == 2){
                        house.facilities.setText(house.facilities.getText() + ", ");
                    }

                }else {
                    if (i == 3){
                        house.facilitiesTwo.setText(item.getFacilities().get(i).getName());
                    }else {
                        house.facilitiesTwo.setText(house.facilitiesTwo.getText() + ", " +
                                item.getFacilities().get(i).getName());
                    }
                    if (i == item.getFacilities().size() - 1){
                        house.facilitiesTwo.setText(house.facilitiesTwo.getText() + "]");
                    }
                }
            }
        }
    }

    /**
     * the model item;
     */
    private class ViewHouse{
        private AnchorPane anchorPane;

        private Label lHouseName;
        private Label houseName;
        private Label noOfRooms;
        private Label rooms;
        private Label fullFacilities;
        private Label facilities;
        private Label facilitiesTwo;
        private Label lRent;
        private Label rent;

        private RadioButton radioButton;

        private Color colorGreen;
        private Font font;

        private ImageView imageView;

        /**
         * the method sets properties;
         */
        private void setData(){
            font = new Font("Verdana", 12);
            colorGreen = Color.GREEN;

            anchorPane = new AnchorPane();

            radioButton = new RadioButton();
            radioButton.setLayoutX(528);
            radioButton.setLayoutY(84);
            radioButton.setCursor(Cursor.HAND);

            lHouseName = new Label("House name: ");
            lHouseName.setLayoutX(252);
            lHouseName.setLayoutY(28);
            lHouseName.setFont(font);

            noOfRooms = new Label("No of rooms: ");
            noOfRooms.setLayoutX(252);
            noOfRooms.setLayoutY(53);
            noOfRooms.setFont(font);

            fullFacilities = new Label("Full facilities: ");
            fullFacilities.setLayoutX(252);
            fullFacilities.setLayoutY(80);
            fullFacilities.setFont(font);

            lRent = new Label("Rent: ");
            lRent.setLayoutX(252);
            lRent.setLayoutY(162);
            lRent.setFont(font);

            houseName = new Label("name");
            houseName.setLayoutX(340);
            houseName.setLayoutY(28);
            houseName.setFont(font);
            houseName.setTextFill(colorGreen);

            rooms = new Label("1");
            rooms.setLayoutX(339);
            rooms.setLayoutY(53);
            rooms.setFont(font);
            rooms.setTextFill(colorGreen);

            facilities = new Label("");
            facilities.setLayoutX(255);
            facilities.setLayoutY(101);
            facilities.setFont(font);
            facilities.setTextFill(colorGreen);
            facilities.setMaxWidth(250);

            facilitiesTwo = new Label("");
            facilitiesTwo.setLayoutX(255);
            facilitiesTwo.setLayoutY(120);
            facilitiesTwo.setFont(font);
            facilitiesTwo.setTextFill(colorGreen);
            facilitiesTwo.setMaxWidth(250);

            rent = new Label("MYR900");
            rent.setLayoutX(295);
            rent.setLayoutY(162);
            rent.setFont(font);
            rent.setTextFill(colorGreen);

            imageView = new ImageView();
            imageView.setLayoutX(14);
            imageView.setLayoutY(12);
            imageView.setFitWidth(230);
            imageView.setFitHeight(175);

            anchorPane.getChildren().addAll(lHouseName, houseName, noOfRooms,
                    rooms, fullFacilities, facilities, lRent, rent, imageView,
                    radioButton, facilitiesTwo);
        }
    }

    /**
     * the method returns selected house;
     * @return selected house;
     */
    static House getSelectedHouse() {
        return selectedHouse;
    }
}
