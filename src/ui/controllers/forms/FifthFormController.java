package ui.controllers.forms;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import objects.Client;
import objects.CreditCard;
import objects.House;
import objects.Paypal;
import resources.Resource;
import ui.WindowLauncher;
import ui.controllers.core.IController;
import utils.DialogManager;
import utils.WorkWithFile;

import java.io.File;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

/**
 * class-controller for the Fifth Form
 */
public class FifthFormController implements IController {

    /**
     * buttons for closing the app and exporting info into .txt
     */
    public Button btnPrint;
    public Button btnClose;

    /**
     * labels for displaying the entered info from previous forms
     */
    public Label lbFullName;
    public Label lbPhoneNumber;
    public Label lbEmailAddress;
    public Label lbPassportNumber;
    public Label lbAddress;
    public Label lbCountry;
    public Label lbCity;
    public Label lbPaymentMethod;
    public Label lbAmount;

    /**
     * objects for exporting data from previous forms
     */
    private House selectedHouse;
    private Client client;
    private Paypal paypal;
    private CreditCard creditCard;

    /**
     * the first method that will be compiled in this class
     * @param location - is not used;
     * @param resources - is not used;
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        DialogManager.showInfoDialog(Resource.Info.INFO, Resource.Info.RESULT);
        setData();
        onClickButtonClose();
        onClickButtonPrint();
    }

    /**
     * the method creates .txt-file and write data into it
     */
    private void onClickButtonPrint() {
        btnPrint.setOnAction(event -> {
            File file = WorkWithFile.createNewFile(new SimpleDateFormat("yyyy_mm_dd_HH_mm_ss").
                    format(new Date()) + ".txt");

            WorkWithFile.writeToFile(file, setResultText());

            WindowLauncher.getINSTANCE().stop();
            openTextFile(file);
        });
    }

    /**
     * the method opens file
     * @param file is the file's name
     */
    private void openTextFile(File file) {
        WorkWithFile.openTextFile(file);
    }

    /**
     * the method creates a string with data for .txt-file
     * @return created string
     */
    private String setResultText() {

        return Resource.ResultText.FULL_NAME + client.getFirstName() +
                " " + client.getMiddleName() + " " +
                client.getLastName() + ";\n" +
                Resource.ResultText.PHONE_NUMBER + client.getPhoneNumber() + ";\n" +
                Resource.ResultText.PASPORT_NUMBER + client.getPassportNumber() + ";\n" +
                Resource.ResultText.EMAIL_ADDRESS + client.getEmailAddress() + ";\n" +
                Resource.ResultText.ADRDRESS + client.getAddressLine1() + " " +
                client.getAddressLine2() + ";\n" +
                Resource.ResultText.CITY + client.getCity() + ";\n" +
                Resource.ResultText.COUNTRY + client.getCountry() + ";\n" +
                Resource.ResultText.PAYMENT_METHOD + (paypal != null ? "Paypal" : "Credit card") +
                "(" + (paypal != null ? "Email: " + paypal.getEmail() :
                "Card type: " + creditCard.getCardType()) + ");\n" +
                Resource.ResultText.AMOUNT + selectedHouse.getPrice().getName() +
                selectedHouse.getPrice().getPrice() + ".";
    }

    /**
     * the method closes app after clicking on close-button
     */
    private void onClickButtonClose() {
        btnClose.setOnAction(event -> WindowLauncher.getINSTANCE().stop());
    }

    /**
     * the method set data into the labels
     */
    private void setData() {
        selectedHouse = SecondFormController.getSelectedHouse();
        client = ThirdFormController.getClient();

        if (FourthFormController.getPaymentData() instanceof Paypal){
            paypal = ((Paypal) FourthFormController.getPaymentData());
        }else if (FourthFormController.getPaymentData() instanceof CreditCard){
            creditCard = ((CreditCard) FourthFormController.getPaymentData());
        }

        if (selectedHouse != null &&
                client != null &&
                (paypal != null || creditCard != null)){
            if (client.getMiddleName() != null) {
                lbFullName.setText(client.getFirstName() + ' ' + client.getMiddleName() + ' ' + client.getLastName());
            } else {
                lbFullName.setText(client.getFirstName() + ' ' + client.getLastName());
            }
            lbPhoneNumber.setText(client.getPhoneNumber());
            lbEmailAddress.setText(client.getEmailAddress());
            lbPassportNumber.setText(client.getPassportNumber());
            if (client.getAddressLine2() != null) {
                lbAddress.setText(client.getAddressLine1() + ", " + client.getAddressLine2());
            }
            lbCity.setText(client.getCity());
            lbCountry.setText(client.getCountry());
            lbPaymentMethod.setText(paypal != null ? "Paypal" + paypal.getEmail() :
                    "Credit card" + creditCard.getCardType());
            lbAmount.setText(selectedHouse.getPrice().getName() +
                    selectedHouse.getPrice().getPrice());
        }
    }
}