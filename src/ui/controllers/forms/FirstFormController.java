package ui.controllers.forms;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import objects.*;
import ui.Resource;
import ui.WindowsService;
import ui.controllers.core.IController;
import utils.CheckData;
import utils.DialogManager;
import utils.DigitalClock;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

/**
 * class-controller for the First Form
 */
public class FirstFormController implements IController {

    /**
     * attributes for image and date-picker
     */
    public ImageView imgOne;
    public DatePicker dpDate;

    /**
     * checkboxes for facilities
     */
    public CheckBox cbWifi;
    public CheckBox cbAirCondition;
    public CheckBox cbTvWithCable;
    public CheckBox cbRentalCars;
    public CheckBox cbSwimmingPool;
    public CheckBox cbBbq;

    /**
     * anchor-pane - the form background
     */
    public AnchorPane anchorPane;

    /**
     * array for storing facilities-chekboxes
     */
    private CheckBox[] checkBoxes;

    /**
     * comboboxes for choosing areas, prices and number of rooms
     */
    public ComboBox<Area> cbArea;
    public ComboBox<PriceRange> cmPrice;
    public ComboBox<Room> cmRooms;

    /**
     * button to next form
     */
    public Button btnSearch;

    /**
     * list and objects that store data about the chosen options
     */
    private ObservableList<Facility> selectedFacilities;
    private Area selectedArea;
    private Month selectedMonth;
    private Room selectedRoom;
    private PriceRange selectedPrice;

    /**
     * list of selected houses (determined by the selected options)
     */
    private static ObservableList<House> selectedHouses;

    /**
     * the first method that will be compiled in this class
     * @param location - is not used;
     * @param resources - is not used;
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setImage();
        setData();
        onClickBtnSearch();
        setCurrentTime();
    }

    /**
     * the method sets text for all nodes (checkboxes, datepicker...)
     */
    private void setData() {
        cbWifi.setText(resources.Resource.FacilitiesCollection.WIFI.getName());
        cbAirCondition.setText(resources.Resource.FacilitiesCollection.AIR_CONDITION.getName());
        cbTvWithCable.setText(resources.Resource.FacilitiesCollection.TV_CABLE.getName());
        cbRentalCars.setText(resources.Resource.FacilitiesCollection.RENTAL_CARS.getName());
        cbSwimmingPool.setText(resources.Resource.FacilitiesCollection.SWIMMING_POOL.getName());
        cbBbq.setText(resources.Resource.FacilitiesCollection.BBQ.getName());

        cbArea.setItems(resources.Resource.AreaCollection.AREAS);
        cmPrice.setItems(resources.Resource.PriceRangeCollection.PRICES);
        cmRooms.setItems(resources.Resource.RoomsCollection.ROOMS);

        dpDate.setValue(LocalDate.now());

        checkBoxes = new CheckBox[]{cbTvWithCable, cbSwimmingPool,
                cbRentalCars, cbBbq, cbAirCondition, cbWifi};
    }

    /**
     * the method switch app to the next form;
     * it also sets selected options to equal objects;
     */
    private void onClickBtnSearch() {
        btnSearch.setOnAction(event -> {
            if (checkValues()) {
                setCheckedFacilities();
                setSelectedArea();
                setSelectedMonth();
                setSelectedRoom();
                setSelectedPrice();

                setSelectedHouses();

                if (!selectedHouses.isEmpty()) {
                    setSecondForm();
                }else {
                    DialogManager.showErrorDialog(resources.Resource.Errors.INFO,
                            resources.Resource.Errors.HOUSE_ISNOT_FOUND);
                }
            }
        });
    }

    /**
     * the method looks for houses that are available with options, chosen by user
     */
    private void setSelectedHouses() {
        selectedHouses = FXCollections.observableArrayList();

        for (House house :
                resources.Resource.HousesCollection.HOUSES) {

            if (house.getArea() == selectedArea){

                for (Month month :
                        house.getAvailability()) {
                    if (month == selectedMonth){

                        if (selectedRoom == resources.Resource.RoomsCollection.ALL ||
                                house.getRoom() == selectedRoom){

                            if (selectedPrice == resources.Resource.PriceRangeCollection.ALL ||
                                    (house.getPrice().getPrice() >= selectedPrice.getMinPrice() &&
                                    house.getPrice().getPrice() <= selectedPrice.getMaxPrice())){

                                int result = 0;
                                for (Facility facility :
                                        house.getFacilities()) {

                                    for (Facility selectedFacility :
                                            selectedFacilities) {
                                        if (facility == selectedFacility){
                                            result++;
                                        }
                                    }
                                }
                                if (result == selectedFacilities.size()) {
                                    selectedHouses.add(house);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * the method sets up the second form
     */
    private void setSecondForm() {
        WindowsService.setWindowSecondForm();
    }

    /**
     * the method sets up selected room
     */
    private void setSelectedRoom() {
        selectedRoom = cmRooms.getSelectionModel().getSelectedItem();
    }

    /**
     * the method sets up selected price
     */
    private void setSelectedPrice() {
        selectedPrice = cmPrice.getSelectionModel().getSelectedItem();
    }

    /**
     * the method sets up selected month
     */
    private void setSelectedMonth() {
        for (Month month :
                Month.values()) {
            if (dpDate.getValue().getMonthValue() == month.getId()){
                selectedMonth = month;
            }
        }
    }

    /**
     * the method sets up selected area
     */
    private void setSelectedArea() {
        selectedArea = cbArea.getSelectionModel().getSelectedItem();
    }

    /**
     * the method sets up checked facilities
     */
    private void setCheckedFacilities() {
        selectedFacilities = FXCollections.observableArrayList();
        for (CheckBox checkBox :
                checkBoxes) {
            if (checkBox.isSelected()){
                for (Facility f :
                        resources.Resource.FacilitiesCollection.FACILITIES) {
                    if (checkBox.getText().equals(f.getName())){
                        selectedFacilities.add(f);
                    }
                }
            }
        }

    }

    /**
     * the method check if all required options was chosen by user
     * @return true if all was chosen correctly, false and Error-message when it was not;
     */
    private boolean checkValues() {
        if (CheckData.checkData(cbArea, cmPrice, cmRooms) &&
                CheckData.checkData(dpDate) &&
                CheckData.checkData(cbAirCondition, cbBbq, cbRentalCars,
                        cbSwimmingPool, cbTvWithCable, cbWifi)){
            return true;
        }

        DialogManager.showErrorDialog(resources.Resource.Errors.ERROR,
                resources.Resource.Errors.ENTER_DATA);
        return false;
    }

    /**
     * the method output image to the form
     */
    private void setImage() {
        imgOne.setImage(Resource.FirstForm.IMAGE);
    }

    /**
     * the method for getting the selected houses
     * @return list of selected houses
     */
    static ObservableList<House> getSelectedHouses() {
        return selectedHouses;
    }

    /**
     * the method for setting up the clock in up right corner of the form
     */
    private void setCurrentTime() {
        DigitalClock digitalClock = new DigitalClock()
                .setProperties(482, 10, 100, 37);

        anchorPane.getChildren().add(digitalClock);
    }
}
