package ui.controllers.forms;

import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import objects.Client;
import objects.Country;
import resources.Resource;
import ui.WindowsService;
import ui.controllers.core.IController;
import utils.CheckData;
import utils.ClearData;
import utils.DialogManager;
import utils.RequestFocus;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * class-controller for the Third Form
 */
public class ThirdFormController implements IController {

    /**
     * text-fields for entering data;
     * combo-box for country selection;
     */
    public TextField tfFirstName;
    public TextField tfMiddleName;
    public TextField tfLastName;
    public TextField tfPhoneNumber;
    public TextField tfEmailAddress;
    public TextField tfPassportNumber;
    public TextField tfAddressLine1;
    public TextField tfAddressLine2;
    public TextField tfCity;

    public ComboBox<Country> cbCountry;

    /**
     * buttons for resetting entered data, returning to previous form and switching to next one
     */
    public Button btnPrevious;
    public Button btnReset;
    public Button btnNext;

    /**
     * object of Client class
     */
    private static Client client;

    /**
     * the first method that will be compiled in this class
     * @param location - is not used;
     * @param resources - is not used;
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        client = null;
        tfFirstName.requestFocus();

        setFocus();
        setData();
        onClickBtnPrevious();
        onClickBtnNext();
        onClickBtnReset();
    }

    /**
     * the method creates a possibility to switch between text-fields with "Enter" key on the keyboard
     */
    private void setFocus() {
        RequestFocus.setRequestFocusOnKeyPressed(tfFirstName, tfMiddleName);
        RequestFocus.setRequestFocusOnKeyPressed(tfMiddleName, tfLastName);
        RequestFocus.setRequestFocusOnKeyPressed(tfLastName, tfPhoneNumber);
        RequestFocus.setRequestFocusOnKeyPressed(tfPhoneNumber, tfEmailAddress);
        RequestFocus.setRequestFocusOnKeyPressed(tfEmailAddress, tfPassportNumber);
        RequestFocus.setRequestFocusOnKeyPressed(tfPassportNumber, tfAddressLine1);
        RequestFocus.setRequestFocusOnKeyPressed(tfAddressLine1, tfAddressLine2);
        RequestFocus.setRequestFocusOnKeyPressed(tfAddressLine2, tfCity);
        RequestFocus.setRequestFocusOnKeyPressed(tfCity, cbCountry);
        RequestFocus.setRequestFocusOnKeyPressed(cbCountry, btnNext);

        btnNext.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER)){
                btnNextAction();
            }
        });
    }

    /**
     * the method returns user to the previous form
     */
    private void onClickBtnPrevious() {
        btnPrevious.setOnAction(event -> setSecondForm());
    }

    /**
     * the method switch user to the next form
     */
    private void onClickBtnNext() {
        btnNext.setOnAction(event -> btnNextAction());
    }

    /**
     * the method checks fields for the entered data, creates client and switch to the next form
     */
    private void btnNextAction() {
        if (checkValues()) {
            setClient();
            setFourthForm();
        }
    }

    /**
     * the method resets all data to its default status
     */
    private void onClickBtnReset() {
        btnReset.setOnAction(event -> clearData());
    }

    /**
     * the method fills the combo-box with countries, declared in Resource-class
     */
    private void setData() {
        cbCountry.setItems(Resource.CountriesCollection.COUNTRIES);
    }

    /**
     * the method creates client-object with all required info
     */
    private void setClient() {
        client = new Client(tfFirstName.getText().trim(), tfMiddleName.getText().trim(), tfLastName.getText().trim(),
                tfPhoneNumber.getText().trim(), tfEmailAddress.getText().trim(), tfPassportNumber.getText().trim(),
                tfAddressLine1.getText().trim(), tfAddressLine2.getText().trim(), tfCity.getText().trim(),
                cbCountry.getSelectionModel().getSelectedItem().getName());
    }

    /**
     * the method sets up the Second Form
     */
    private void setSecondForm() {
        WindowsService.setWindowSecondForm();
    }

    /**
     * the method sets up the Fourth Form
     */
    private void setFourthForm() {
        WindowsService.setWindowFourthForm();
    }

    /**
     * the method clears all text-fields and combo-box
     */
    private void clearData() {
        ClearData.clearData(tfFirstName, tfMiddleName, tfLastName,
                tfPhoneNumber, tfEmailAddress, tfAddressLine1,
                tfAddressLine2, tfCity);

        ClearData.clearData(cbCountry);
    }

    /**
     * the method check if all required options was chosen by user
     * @return true if all was chosen correctly, false and Error-message when it was not
     */
    private boolean checkValues() {
        if (CheckData.checkData(tfFirstName, tfLastName,
                tfPhoneNumber, tfEmailAddress, tfPassportNumber,
                tfAddressLine1, tfCity) &&
                CheckData.checkData(cbCountry)) {
            return true;
        }

        DialogManager.showErrorDialog(resources.Resource.Errors.ERROR,
                resources.Resource.Errors.ENTER_DATA);
        return false;
    }

    /**
     * the method allows to access entered data
     * @return the object of the Client-class
     */
    static Client getClient() {
        return client;
    }
}
