package ui.controllers.forms;

import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import objects.CreditCard;
import objects.Paypal;
import resources.Resource;
import ui.WindowsService;
import ui.controllers.core.IController;
import utils.CheckData;
import utils.ClearData;
import utils.DialogManager;
import utils.DisableNode;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * class-controller for the Fourth Form
 */
public class FourthFormController implements IController {

    /**
     * radiobuttons for choosing the Payment Method
     */
    public RadioButton rbtnCreditCard;
    public RadioButton rbtnPaypal;

    /**
     * radiobuttons for choosing the Card Type
     */
    public RadioButton rbtnVisa;
    public RadioButton rbtnMastercard;
    public RadioButton rbtnAmericanExpress;

    public ToggleGroup tgPaymentMethod = new ToggleGroup();
    public ToggleGroup tgCardType = new ToggleGroup();

    /**
     * text-fields for entering detail payment info
     */
    public TextField tfCardNo;
    public TextField tfCVC;
    public TextField tfExpiry;
    public TextField tfEmail;

    /**
     * text-area shows the total amount that need to be payed
     */
    public TextArea taTotalAmount;

    /**
     * checkbox for user agreement
     */
    public CheckBox cbAgreement;

    /**
     * buttons for resetting entered data, returning to previous form and switching to next one
     */
    public Button btnReset;
    public Button btnPrevious;
    public Button btnSubmit;

    /**
     * panes for card types and credit-card details
     */
    public AnchorPane paneCardType;
    public AnchorPane panePayment;

    /**
     * attribute for storing data about the card's type
     */
    private String cardType;

    /**
     * objects for storing payment data
     */
    private static Paypal paypal;
    private static CreditCard creditCard;

    /**
     * the first method that will be compiled in this class
     * @param location - is not used;
     * @param resources - is not used;
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        paypal = null;
        creditCard = null;

        setTotalAmount();
        setNodesDisabled();
        onClickPaymentMethod();
        onClickCardType();
        onClickBtnPrevious();
        onClickBtnReset();
        onClickBtnSubmit();
    }

    /**
     * the method displays total amount based on entered in previous forms data
     */
    private void setTotalAmount() {
        if (SecondFormController.getSelectedHouse() != null) {
            taTotalAmount.setText("Total Amount:\n" + SecondFormController.getSelectedHouse().getPrice().getName() +
                    SecondFormController.getSelectedHouse().getPrice().getPrice());
        }
    }

    /**
     * the method disables editing of chosen nodes (text-fields, radiobuttons)
     */
    private void setNodesDisabled() {
        DisableNode.setDisableNode(true, paneCardType, panePayment, tfEmail);
    }

    /**
     * the method enables entering paypal-email
     */
    private void onClickPaymentMethod() {
        rbtnPaypal.setOnAction(event -> {
            DisableNode.setDisableNode(false, tfEmail);
            DisableNode.setDisableNode(true, paneCardType, panePayment);
        });
    }

    /**
     * the method enables choosing the card's type
     */
    private void onClickCardType() {
        rbtnCreditCard.setOnAction(event -> {
            ablePaymentInfo();
            DisableNode.setDisableNode(true, tfEmail);
        });

        rbtnVisa.setOnAction(event -> cardType = "Visa");

        rbtnMastercard.setOnAction(event -> cardType = "Mastercard");

        rbtnAmericanExpress.setOnAction(event -> cardType = "AmericanExpress");
    }

    /**
     * the method enables nodes for entering more detail data about the credit card
     */
    private void ablePaymentInfo() {
        DisableNode.setDisableNode(false, paneCardType, panePayment);
    }

    /**
     * the method returns user to the previous form
     */
    private void onClickBtnPrevious() {
        btnPrevious.setOnAction(event -> setThirdForm());
    }

    /**
     * the method resets data to its default status
     */
    private void onClickBtnReset() {
        btnReset.setOnAction(event -> {

            ClearData.clearData(tfCardNo, tfExpiry,
                    tfCVC, tfEmail);

            ClearData.clearData(cbAgreement);
        });
    }

    /**
     * the method switch user to the next form
     */
    private void onClickBtnSubmit() {
        btnSubmit.setOnAction(event -> {
            if (checkValues()) {
                setFifthForm();
            }
        });
    }

    /**
     * the method sets up Third Form
     */
    private void setThirdForm() {
        WindowsService.setWindowThirdForm();
    }

    /**
     * the method sets up Fifth Form
     */
    private void setFifthForm() {
        WindowsService.setWindowFifthForm();
    }

    /**
     * the method check if all required options was chosen by user
     * @return true if all was chosen correctly, false and Error-message when it was not
     */
    private boolean checkValues() {
        if (rbtnCreditCard.isSelected()) {
            if (CheckData.checkData(tfCardNo, tfCVC, tfExpiry) &&
                    CheckData.checkData(cbAgreement) &&
                    CheckData.checkData(rbtnVisa, rbtnMastercard, rbtnAmericanExpress)) {

                creditCard = new CreditCard(cardType, tfCardNo.getText().trim(), tfCVC.getText().trim(),
                        tfExpiry.getText().trim());
                return true;
            } else {
                DialogManager.showErrorDialog(Resource.Errors.ERROR, Resource.Errors.ENTER_DATA);
            }
        }

        if (rbtnPaypal.isSelected()) {
            if (CheckData.checkData(tfEmail) &&
                    CheckData.checkData(cbAgreement)) {

                paypal = new Paypal(tfEmail.getText().trim());
                return true;
            } else {
                DialogManager.showErrorDialog(Resource.Errors.ERROR, Resource.Errors.ENTER_DATA);
            }
        }

        DialogManager.showErrorDialog(Resource.Errors.ERROR, Resource.Errors.ENTER_DATA);
        return false;
    }

    /**
     * the method allows to access the entered payment data
     * @return paypal or creditCard object
     */
    static Object getPaymentData() {
        if(creditCard == null) {
            return paypal;
        } else {
            return creditCard;
        }
    }
}
