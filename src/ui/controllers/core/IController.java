package ui.controllers.core;

import javafx.fxml.Initializable;

/**
 * Interface for all classes-controllers that implements Initializable-interface (makes to override method initialize,
 * which is initializing first in the compiling-process)
 */
public interface IController extends Initializable{
}
