package ui;

import ui.core.ASceneConstructor;
import ui.scenes.forms.*;

/**
 * the class for changing scenes;
 */
public final class WindowsService {

    /**
     * the main stage where will be changing scenes;
     */
    private static WindowLauncher windowLauncher = WindowLauncher.getINSTANCE();

    /**
     * the scenes with properties
     */
    private static ASceneConstructor firstForm = new FirstForm();
    private static ASceneConstructor secondForm = new SecondForm();
    private static ASceneConstructor thirdForm = new ThirdForm();
    private static ASceneConstructor fourthForm = new FourthForm();
    private static ASceneConstructor fifthForm = new FifthForm();

    /**
     * the method sets first form;
     */
    public static void setWindowFirstForm() {
        windowLauncher.setWindow(Resource.FirstForm.WINDOW_MIN_WIDTH,
                Resource.FirstForm.WINDOW_MIN_HEIGHT,
                Resource.FirstForm.TITLE,
                Resource.FirstForm.IS_RESIZABLE,
                firstForm.setWindowScene());
    }

    /**
     * the method sets second form;
     */
    public static void setWindowSecondForm() {
        windowLauncher.setWindow(Resource.SecondForm.WINDOW_MIN_WIDTH,
                Resource.SecondForm.WINDOW_MIN_HEIGHT,
                Resource.SecondForm.TITLE,
                Resource.SecondForm.IS_RESIZABLE,
                secondForm.setWindowScene());
    }

    /**
     * the method sets third form;
     */
    public static void setWindowThirdForm() {
        windowLauncher.setWindow(Resource.ThirdForm.WINDOW_MIN_WIDTH,
                Resource.ThirdForm.WINDOW_MIN_HEIGHT,
                Resource.ThirdForm.TITLE,
                Resource.ThirdForm.IS_RESIZABLE,
                thirdForm.setWindowScene());
    }

    /**
     * the method sets fourth form;
     */
    public static void setWindowFourthForm() {
        windowLauncher.setWindow(Resource.FourthForm.WINDOW_MIN_WIDTH,
                Resource.FourthForm.WINDOW_MIN_HEIGHT,
                Resource.FourthForm.TITLE,
                Resource.FourthForm.IS_RESIZABLE,
                fourthForm.setWindowScene());
    }

    /**
     * the method sets fifth form;
     */
    public static void setWindowFifthForm() {
        windowLauncher.setWindow(Resource.FifthForm.WINDOW_MIN_WIDTH,
                Resource.FifthForm.WINDOW_MIN_HEIGHT,
                Resource.FifthForm.TITLE,
                Resource.FifthForm.IS_RESIZABLE,
                fifthForm.setWindowScene());
    }
}
