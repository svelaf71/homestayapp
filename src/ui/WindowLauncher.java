package ui;

import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import ui.core.IWindowLauncher;

/**
 * the class for working with stage(e.g. displaying, stopping, etc.);
 */
public final class WindowLauncher implements IWindowLauncher {

    private Stage window;

    /**
     * pattern singleton, the main stage;
     */
    private static final WindowLauncher INSTANCE = new WindowLauncher();

    private WindowLauncher() {
        this.window = new Stage();
    }

    public static WindowLauncher getINSTANCE() {
        return INSTANCE;
    }

    @Override
    public void setPrefSize(double width, double height) {
        window.setMinHeight(height);
        window.setMinWidth(width);
        window.setWidth(width);
        window.setHeight(height);
    }

    @Override
    public void setTitle(String title) {
        window.setTitle(title);
    }

    @Override
    public void setScene(Scene scene) {
        window.setScene(scene);
    }

    @Override
    public void setIcons(String url) {
        window.getIcons().add(new Image(url));
    }

    @Override
    public void setResizable(boolean isResizable) {
        window.setResizable(isResizable);
    }

    @Override
    public void display() {
        window.show();
    }

    @Override
    public void stop() {
        window.close();
    }

    @Override
    public void setWindow(double width, double height, String title,
                          boolean isResizable, Scene scene){
        setScene(scene);
        setPrefSize(width, height);
        setTitle(title);
        setResizable(isResizable);
        window.setX(50);
        window.setY(50);
    }
}
