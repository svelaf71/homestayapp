package ui;

import javafx.scene.image.Image;

/**
 * the class contains other classes with information about forms(e.g. tittle width, height etc.);
 */
public class Resource {

    /**
     * information about first form;
     */
    public static final class FirstForm {

        public static final String FILE_NAME = "/forms/firstForm.fxml";
        static final String TITLE = "FirstForm";
        static final boolean IS_RESIZABLE = false;
        static final double WINDOW_MIN_WIDTH = 600;
        static final double WINDOW_MIN_HEIGHT = 640;

        static final String IMAGE_URL = "/resources/images/Homestay-House.jpg";
        public static final Image IMAGE = new Image(IMAGE_URL);

    }

    /**
     * information about second form;
     */
    public static final class SecondForm {
        public static final String FILE_NAME = "/forms/secondForm.fxml";
        static final String TITLE = "SecondForm";
        static final boolean IS_RESIZABLE = false;
        static final double WINDOW_MIN_WIDTH = 600;
        static final double WINDOW_MIN_HEIGHT = 640;
    }

    /**
     * information about third form;
     */
    public static final class ThirdForm {
        public static final String FILE_NAME = "/forms/thirdForm.fxml";
        static final String TITLE = "ThirdForm";
        static final boolean IS_RESIZABLE = false;
        static final double WINDOW_MIN_WIDTH = 600;
        static final double WINDOW_MIN_HEIGHT = 640;
    }

    /**
     * information about fourth form;
     */
    public static final class FourthForm {
        public static final String FILE_NAME = "/forms/fourthForm.fxml";
        static final String TITLE = "FourthForm";
        static final boolean IS_RESIZABLE = false;
        static final double WINDOW_MIN_WIDTH = 600;
        static final double WINDOW_MIN_HEIGHT = 640;
    }

    /**
     * information about fifth form;
     */
    public static final class FifthForm {
        public static final String FILE_NAME = "/forms/fifthForm.fxml";
        static final String TITLE = "FifthForm";
        static final boolean IS_RESIZABLE = false;
        static final double WINDOW_MIN_WIDTH = 600;
        static final double WINDOW_MIN_HEIGHT = 640;
    }
}
