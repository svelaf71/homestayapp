package ui.core;

import javafx.scene.Parent;

import java.io.IOException;
import java.util.Locale;

/**
 * interface where is method for connecting to fxml files;
 */
public interface IFXMLFileConnection {

    /**
     * the method creates new Parent object
     * @param fileName - name file;
     * @return - new Parent object;
     * @throws IOException
     */
    Parent connectToFXMLFile(String fileName) throws IOException;

}
