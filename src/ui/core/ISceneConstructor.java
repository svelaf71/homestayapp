package ui.core;

import javafx.scene.Scene;

import java.util.Locale;

/**
 * the interface where is method for creating new scene;
 */
public interface ISceneConstructor {

    /**
     * the method creates new scene;
     * @return created scene;
     */
    Scene setWindowScene();
}
