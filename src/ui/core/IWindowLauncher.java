package ui.core;

import javafx.scene.Scene;

/**
 * the interface where describes methods for working with stage;
 */
public interface IWindowLauncher {

    /**
     * the method sets prefSize
     * @param width - prefWidth;
     * @param height - prefHeight;
     */
    void setPrefSize(double width, double height);

    /**
     * the method sets the title;
     * @param title - the title of stage;
     */
    void setTitle(String title);

    /**
     * the method sets the scene;
     * @param scene
     */
    void setScene(Scene scene);

    /**
     * the method sets icon of stage;
     * @param url - path of image;
     */
    void setIcons(String url);

    /**
     * the method sets stage resizable or no resizable;
     * @param isResizable
     */
    void setResizable(boolean isResizable);

    /**
     * the method sets scene with next properties
     * @param width
     * @param height
     * @param title
     * @param isResizable
     * @param scene
     */
    void setWindow(double width, double height, String title,
                   boolean isResizable, Scene scene);

    /**
     * the method displays stage;
     */
    void display();

    /**
     * the methods stops displaying stage;
     */
    void stop();
}
