package ui.core;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * the class for creating new Parent object;
 */
public class FXMLFileConnection implements IFXMLFileConnection {

    @Override
    public Parent connectToFXMLFile(String fileName) throws IOException {

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(fileName));

        return fxmlLoader.load();
    }
}
