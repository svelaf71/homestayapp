package ui.core;

import javafx.scene.Scene;

/**
 * the class where're objects for connecting to fxml files and scene objects
 */
public abstract class ASceneConstructor implements ISceneConstructor {
    private FXMLFileConnection fxmlFileConnection;
    private Scene scene;

    protected FXMLFileConnection getFxmlFileConnection() {
        return fxmlFileConnection;
    }

    protected void setFxmlFileConnection(FXMLFileConnection fxmlFileConnection) {
        this.fxmlFileConnection = fxmlFileConnection;
    }

    public Scene getScene() {
        return scene;
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }
}
