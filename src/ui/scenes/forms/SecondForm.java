package ui.scenes.forms;

import javafx.scene.Scene;
import ui.Resource;
import ui.core.ASceneConstructor;
import ui.core.FXMLFileConnection;

import java.io.IOException;

/**
 * the class for creating second form
 */
public class SecondForm extends ASceneConstructor {

    /**
     * the method creates first form scene
     * @return new first form
     */
    @Override
    public Scene setWindowScene() {

        try {
            setFxmlFileConnection(new FXMLFileConnection());
            setScene(new Scene(getFxmlFileConnection().
                    connectToFXMLFile(Resource.SecondForm.FILE_NAME)));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return getScene();
    }
}
