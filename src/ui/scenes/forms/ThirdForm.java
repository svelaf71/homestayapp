package ui.scenes.forms;

import javafx.scene.Scene;
import ui.Resource;
import ui.core.ASceneConstructor;
import ui.core.FXMLFileConnection;

import java.io.IOException;

/**
 * the class for creating third form
 */
public class ThirdForm extends ASceneConstructor {

    /**
     * the method creates third form scene
     * @return new third form
     */
    @Override
    public Scene setWindowScene() {

        try {
            setFxmlFileConnection(new FXMLFileConnection());
            setScene(new Scene(getFxmlFileConnection().
                    connectToFXMLFile(Resource.ThirdForm.FILE_NAME)));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return getScene();
    }
}
