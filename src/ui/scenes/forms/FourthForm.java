package ui.scenes.forms;

import javafx.scene.Scene;
import ui.Resource;
import ui.core.ASceneConstructor;
import ui.core.FXMLFileConnection;

import java.io.IOException;

/**
 * Created by Andrew on 18.03.2016.
 */
public class FourthForm extends ASceneConstructor {

    /**
     * the method fourth first form scene
     * @return new fourth form
     */
    @Override
    public Scene setWindowScene() {

        try {
            setFxmlFileConnection(new FXMLFileConnection());
            setScene(new Scene(getFxmlFileConnection().
                    connectToFXMLFile(Resource.FourthForm.FILE_NAME)));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return getScene();
    }
}
