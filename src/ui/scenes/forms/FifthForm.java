package ui.scenes.forms;

import javafx.scene.Scene;
import ui.Resource;
import ui.core.ASceneConstructor;
import ui.core.FXMLFileConnection;

import java.io.IOException;

/**
 * the class for creating fifth form;
 */
public class FifthForm extends ASceneConstructor {

    /**
     * the method creates fifth form scene
     * @return new fifth form
     */
    @Override
    public Scene setWindowScene() {

        try {
            setFxmlFileConnection(new FXMLFileConnection());
            setScene(new Scene(getFxmlFileConnection().
                    connectToFXMLFile(Resource.FifthForm.FILE_NAME)));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return getScene();
    }
}
