package applications;

import javafx.application.Application;
import javafx.stage.Stage;
import ui.WindowLauncher;
import ui.WindowsService;
import utils.DialogManager;

import java.io.IOException;

/**
 * the class runs the application;
 */
public class Main extends Application {

    public static void main(String[] args){
        launch(args);
    }

    /**
     * the method run the method initForm;
     * @param primaryStage - this stage is not used;
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        initForm();
    }

    /**
     * the method run the first form;
     * @throws IOException
     */
    private void initForm() throws IOException {
        WindowsService.setWindowFirstForm();
        WindowLauncher.getINSTANCE().display();
    }
}